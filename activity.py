# # Capstone Specifications

# The goal of the capstone project is to create a simple employee ticketing system using classes and objects.

# 1. Create a Person class that is an abstract class that has the following methods:
from abc import ABC, abstractclassmethod

class Person(ABC):
	@abstractclassmethod
# 	a. getFullName method
	def getFullName(self):
		pass
# 	b. addRequest method
	def addRequest(self):
		pass
# 	c. checkRequest method
	def checkRequest(self):
		pass
# 	d. addUser method
	def addUser(self):
		pass

# 2. Create an Employee class from Person with the following properties and methods:
class Employee(Person):
# a. Properties (Make sure they are private and have getters/setters)
	def __init__(self, fname, lname, email, dept):
# 	i. firstName
		self._fname = fname
# 	ii. lastName
		self._lname = lname
# 	iii. email
		self._email = email
# 	iv. department
		self._dept = dept
		# self._reqNo = 0
	# Getters
# 	i. firstName
	def get_fname(self, fname):
		print(f"Employee First Name: {self._fname}")
# 	ii. lastName
	def get_lname(self, lname):
		print(f"Employee Last Name: {self._lname}")
# 	iii. email
	def get_email(self, email):
		print(f"Employee Email: {self._email}")
# 	iv. department
	def get_dept(self, dept):
		print(f"Employee Department: {self._dept}")
	# Setters
# 	i. firstName
	def set_fname(self, fname):
		self._fname = fname
# 	ii. lastName
	def set_lname(self, lname):
		self._lname = lname
# 	iii. email
	def set_email(self, email):
		self._email = email
# 	iv. department
	def set_dept(self, dept):
		self._dept = dept
# b. Methods
	def getFullName(self):
		fullName = str(self._fname+" "+self._lname)
		# print(f"{self._fname} {self._lname}")
		return fullName
	def addRequest(self):
		# str(self._lname+"_"+self._fname+"_request"+self._reqNo) = Request(str(self._lname+"_"+self._fname+"_request"+self._reqNo),str(self._lname+"_"+self._fname),)
		reqMsg = "Request has been added" 
		# print(reqMsg)
		return reqMsg
# c. Abstract methods (All methods just return Strings of simple text)
# 	i. checkRequest() - placeholder method
	def checkRequest(self):
		pass
# 	ii. addUser() - placeholder method
	def addUser(self):
		pass
# 	iii. login() - outputs "<Email> has logged in"
	def login(self):
		loginMsg = str(self._email+" has logged in")
		# print(loginMsg)
		return loginMsg

# 	iv. logout() - outputs "<Email> has logged out"
	def logout(self):
		logoutMsg = str(self._email+" has logged out")
		# print(logoutMsg)
		return logoutMsg

# 3. Create a TeamLead class from Person with the following properties and methods:
class Member(Person):
	def __init__(self, fname, lname, email, dept):
# 	i. firstName
		self._fname = fname
# 	ii. lastName
		self._lname = lname
# 	iii. email
		self._email = email
# 	iv. department
		self._dept = dept
class TeamLead(Employee):
	def __init__(self, fname, lname, email, dept):
		super().__init__(fname, lname, email, dept)
		self._members = []

# a. Properties (Make sure they are private and have getters/setters)
# 	i. firstName
# 	ii. lastName
# 	iii. email
# 	iv. department

# b. Methods
	def getFullName(self):
		fullName = str(self._fname+" "+self._lname)
		# print(f"{self._fname} {self._lname}")
		return fullName
	def get_members(self):
		return self._members
# c. Abstract methods (All methods just return Strings of simple text)
# 	i. checkRequest() - placeholder method
# 	ii. addUser() - placeholder method
# 	iii. login() - outputs "<Email> has logged in"
# 	iv. logout() - outputs "<Email> has logged out"
# 	v. addMember() - adds an employee to the members list
	def addMember(self, member):
		self._members.append(member)


# 4. Create an Admin class from Person with the following properties and methods:
class Admin(Employee):
	def __init__(self, fname, lname, email, dept):
		super().__init__(fname, lname, email, dept)
# a. Properties(make sure they are private and have getters/setters)
# 	i. firstName
# 	ii. lastName
# 	iii. email
# 	iv. department
# b. Methods
	def getFullName(self):
		fullName = str(self._fname+" "+self._lname)
		# print(f"{self._fname} {self._lname}")
		return fullName
# c. Abstract methods (All methods just return Strings of simple text)
# 	i. checkRequest() - placeholder method
# 	ii. addUser() - placeholder method
# 	iii. login() - outputs "<Email> has logged in"
# 	iv. logout() - outputs "<Email> has logged out"
# 	v. addUser() - outputs "New user added”
	def addUser(self):
		addMsg = "User has been added"
		return addMsg
# 5. Create a Request class that has the following properties and methods:
class Request():

# a. Properties
	def __init__(self, reqName, requester, dateRequested):
# 	i. name
		self._reqName = reqName
# 	ii. requester
		self._requester = requester
# 	iii. dateRequested
		self._dateRequested = dateRequested
# 	iv. status
		self._status = "pending"
	# Getters
	def get_requestName(self):
		print(f"{self._reqName}")
	def get_requester(self):
		print(f"{self._requester}")
	def get_dateRequested(self):
		print(f"{self._dateRequested}")
	def get_status(self):
		print(f"{self._status}")
	# Setters
	def set_requestName(self):
		self._reqName = reqName
	def set_requester(self):
		self._requester = requester
	def set_dateRequested(self):
		self._dateRequested = dateRequested
	def set_status(self, status):
		self._status = status
# b. Methods
# 	i. updateRequest
	def updateRequest(self):
		updateMsg = f"Request {self._reqName} has been updated"
		return updateMsg
# 	ii. closeRequest
	def closeRequest(self):
		self._status = "closed"
		closeMsg = f"Request {self._reqName} has been closed"
		return closeMsg
# 	iii. cancelRequest
	def cancelRequest(self):
		self._status = "cancelled"
		cancelMsg = f"Request {self._reqName} has been cancelled"
		return cancelMsg

# Test Cases
employee1 = Employee("John", "Doe", "djohn@mail.com", "Marketing")
employee2 = Employee("Jane", "Smith", "sjane@mail.com", "Marketing")
employee3 = Employee("Robert", "Patterson", "probert@mail.com", "Sales")
employee4 = Employee("Brandon", "Smith", "sbrandon@mail.com", "Sales")
admin1 = Admin("Monika", "Justin", "jmonika@mail.com", "Marketing")
teamlead1 = TeamLead("Michael", "Specter", "smichael@mail.com", "Sales")
req1 = Request("New hire operation", teamlead1, "27-Jul-2021")
req2 = Request("Laptop repair", teamlead1, "1-Jul-2021")

# Capstone Specifications
assert employee1.getFullName() == "John Doe", "Full name should be John Doe"
assert admin1.getFullName() == "Monika Justin", "Full name should be Monika Justin"
assert teamlead1.getFullName() == "Michael Specter", "Full name should be Michael Specter"
assert employee2.login() == "sjane@mail.com has logged in"
assert employee2.addRequest() == "Request has been added"
assert employee2.logout() == "sjane@mail.com has logged out"

teamlead1.addMember(employee3)
teamlead1.addMember(employee4)
for indiv_emp in teamlead1.get_members():
	print(indiv_emp.getFullName())

assert admin1.addUser() == "User has been added"

req2.set_status("closed")
print(req2.closeRequest())